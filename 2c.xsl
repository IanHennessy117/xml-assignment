<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>Order Details</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Item Description</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Item Code</th>
                        </tr>
                  
                           <xsl:for-each select="orders/order/history/status[type=dispatched]">
                               <xsl:for-each select="../../../items/item">
                                <tr>
                            <td><xsl:value-of select="description"/></td>
                            <td><xsl:value-of select="quantity"/></td> 
                            <td><xsl:value-of select="price"/></td> 
                            <td><xsl:value-of select="code"/></td> 
                                </tr>
                               </xsl:for-each>
                            </xsl:for-each>
                                     
                
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>